//
//  WorkTimeHandler.h
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DayData.h"
#import "WorkRecord.h"

@interface WorkTimeHandler : NSObject {
    NSMutableArray *customers;
}

-(WorkTimeHandler*)init;
-(NSMutableArray*) getCustomers;
-(DayData*) getCurrentDayData;
-(NSMutableArray*) getMonthDayData;
-(WorkRecord*) getCurrentActiveWorkRecord;

@end
