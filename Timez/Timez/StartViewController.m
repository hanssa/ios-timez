//
//  StartViewController.m
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StartViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface StartViewController ()

@end

@implementation StartViewController
@synthesize knappen;
@synthesize customerPicker;

#define LOGGEDOUT 0 
#define LOGGEDIN 1 



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        knappen.tag = LOGGEDOUT;  
        [self configureToolBar];
        
        
    }
    
    ///Probably check here or early if we have more than one possible customer.
    ///choose other display than picker if we only have one.. label
    
    return self;
}

- (IBAction)startLogging:(id)sender
{
     [self prettyPlease];
    //[(UIButton *)sender set:[UIColor greenColor]];
}

- (void)prettyPlease
{
    if (knappen.tag == LOGGEDOUT)
    {
        knappen.tag = LOGGEDIN;
        [knappen setTitle:@"Logga tid" forState:UIControlStateNormal];
        [knappen setImage:[UIImage imageNamed:@"button_green_large.png"] forState:UIControlStateNormal];
        [knappen setImage:[UIImage imageNamed:@"button_green_small.png"] forState:UIControlEventTouchDown];
        
        [self login:picker_selection];
    } else {
        knappen.tag = LOGGEDOUT;
        [knappen setTitle:@"Logga ut" forState:UIControlStateNormal];
        [knappen setImage:[UIImage imageNamed:@"button_red_large.png"] forState:UIControlStateNormal];
        [knappen setImage:[UIImage imageNamed:@"button_red_small.png"] forState:UIControlEventTouchDown];
        [self logout];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do some button configurations
    knappen.layer.borderWidth = 0.0;
    [knappen setTitle:@"Logga tid" forState:UIControlStateNormal];
    [knappen setImage:[UIImage imageNamed:@"button_red_large.png"] forState:UIControlStateNormal];
    [knappen setImage:[UIImage imageNamed:@"button_red_small.png"] forState:UIControlEventTouchDown];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setKnappen:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [customerPicker release];
    [knappen release];
    [super dealloc];
}
#pragma mark Picker DataSource/Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 5;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    ///get array of possible customers and active/deactive from ?
    ///display customers in different colors depending on active/deactive and default
    
    switch (row){
        case 1:
            return @"Picker 1";
            break;
        case 2:
            return @"Picker 2";

            break;
        case 3:
            return @"Picker 3";

            break;
        case 4:
            return @"Picker 4";

            break;
        default:
            return @"Picker 5";

            break;
    }
    return @"error";
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"choosen row %d",row);
    
    picker_selection = row;
}

#pragma mark Log shechit
-(void)login:(int)id
{
    
}

-(void)logout
{
    
}

@end
